.ALIASES
V_V1            V1(+=ENTRADA -=0 ) CN @EJ1_2.SCHEMATIC1(sch_1):INS32@SOURCE.VSIN.Normal(chips)
D_D1            D1(1=ENTRADA 2=SALIDA ) CN @EJ1_2.SCHEMATIC1(sch_1):INS69@EDIODE.1N4148.Normal(chips)
R_R1            R1(1=0 2=SALIDA ) CN @EJ1_2.SCHEMATIC1(sch_1):INS94@ANALOG.R.Normal(chips)
_    _(Entrada=ENTRADA)
_    _(Salida=SALIDA)
.ENDALIASES
