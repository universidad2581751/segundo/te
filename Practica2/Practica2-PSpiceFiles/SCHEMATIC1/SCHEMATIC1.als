.ALIASES
Q_Q1            Q1(c=N00186 b=N00190 e=N00231 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS28@BIPOLAR.PN2222A.Normal(chips)
R_R1            R1(1=N00186 2=N00235 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS55@ANALOG.R.Normal(chips)
R_R2            R2(1=N00190 2=N00235 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS71@ANALOG.R.Normal(chips)
R_R3            R3(1=N00227 2=N00231 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS87@ANALOG.R.Normal(chips)
R_R4            R4(1=0 2=N00190 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS103@ANALOG.R.Normal(chips)
V_V1            V1(+=N00235 -=0 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS142@SOURCE.VDC.Normal(chips)
R_R5            R5(1=0 2=N00227 ) CN @PRACTICA2.SCHEMATIC1(sch_1):INS168@ANALOG.R.Normal(chips)
.ENDALIASES
