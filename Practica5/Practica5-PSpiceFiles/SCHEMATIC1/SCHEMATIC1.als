.ALIASES
R_R1            R1(1=VI 2=VO ) CN @PRACTICA5.SCHEMATIC1(sch_1):INS69@ANALOG.R.Normal(chips)
C_C1            C1(1=0 2=VO ) CN @PRACTICA5.SCHEMATIC1(sch_1):INS94@ANALOG.C.Normal(chips)
V_V2            V2(+=VI -=0 ) CN @PRACTICA5.SCHEMATIC1(sch_1):INS257@SOURCE.VAC.Normal(chips)
_    _(Vi=VI)
_    _(Vo=VO)
.ENDALIASES
